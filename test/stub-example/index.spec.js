'use strict';
const Path = require("path");
const sinon = require("sinon");



const stubExample = require(Path.join(process.cwd(), "src/stub-example/index"));

describe("promiseChaining", () => {
  const sandbox = sinon.sandbox.create();

  beforeEach(() => {

  });
  afterEach(() => {
    sandbox.restore();
  });
  it.skip("getUserId", () => {
    sinon.stub(stubExample, "getMyName").returns("Shankar")
    const output = stubExample.getMyName();

    console.log(output);
  });

/*
I want to stub getMyName() which is called from inside printMyName()
This is not working

*/
  it("printMyName", () => {
    //sinon.stub(stubExample, "getMyName").returns("Shankar")

    let getMyName = sinon.stub();
    getMyName.returns("Shankar");
    const output = stubExample.printMyName();
    console.log(output);
  })


})
