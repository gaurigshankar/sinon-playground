
const name = "Gauri";
const getMyName = () => {
  return name;
}

const printMyName = () => {
  const myName = getMyName();
  console.log(myName);
  return myName;
}

module.exports = {
  getMyName,
  printMyName
}
